import 'package:csadmin/ui/screens/home_screen/bloc/tab_bar_bloc.dart';
import 'package:csadmin/ui/screens/home_screen/home.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'core/utilities/constants.dart';


void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        systemNavigationBarColor: Colors.transparent,
        systemNavigationBarDividerColor: Colors.transparent,
        statusBarColor: kLightBlueColor,
      ),
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HitBitz Admin',
      // home: HomePage(),
      home: BlocProvider<TabBarBloc>(
        create: (blocProviderContext) => TabBarBloc(),
        child: const HomePage(),
      ),
    );
  }
}
