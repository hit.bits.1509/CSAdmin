class UserModel {
  late String username;
  late String number;
  late String firstName;
  late String lastName;
  late String? status;

  UserModel({
    required this.firstName,
    required this.lastName,
    required this.username,
    required this.number,
    this.status = 'free',
  });
  UserModel.fromJson(Map<dynamic, dynamic> map) {
    if (map == null) {
      return;
    }

    firstName = map['firstname'];
    lastName = map['lastName'];
    number = map['number'];
    username = map['username'];
    status = map['status'];
  }
  toJson() {
    return {
      'status': status,
      'firstName': firstName,
      'lastName': lastName,
      'username': username,
      'status': status
    };
  }
}
