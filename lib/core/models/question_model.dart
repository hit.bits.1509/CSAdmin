import 'choice_model.dart';

class Question {
  late String text;
  late String subject;
  late String department;
  List<Choice> choices = List.generate(
    4,
    (index) => Choice(isTrue: false, text: ''),
  );

  Question({
    required this.subject,
    required this.department,
    required this.text,
    required this.choices,
  });
  Question.fromJson(Map<dynamic, dynamic> map) {
    subject = map['subject'];
    department = map['department'];
    text = map['text'];
    choices = map['choices'];
  }
}
