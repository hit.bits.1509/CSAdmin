import 'package:flutter/cupertino.dart';

class Choice extends ChangeNotifier {
  late String text;
  late bool isTrue;
  Choice({
    required this.isTrue,
    required this.text,
  });
  Choice.fromJson(Map<dynamic, dynamic> map) {
    if (map == null) {
      return;
    }
    text = map['text'];
    isTrue = map['isTrue'];
  }
  toJson() {
    return {'text': text, 'isTrue': isTrue};
  }

  void toggleTrue() {
    isTrue = !isTrue;
    notifyListeners();
  }
}
