import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'app_bar_state.dart';
part 'app_bar_event.dart';

class AppBarBloc extends Bloc<AppBarEvent, AppBarState> {
  AppBarBloc() : super(AppBarMainState()) {
    on<AppBarMainEvent>((event, emit) {
      emit(AppBarMainState());
    });
    on<AppBarMenuEvent>((event, emit) {
      emit(AppBarMenuState());
    });
  }
}
