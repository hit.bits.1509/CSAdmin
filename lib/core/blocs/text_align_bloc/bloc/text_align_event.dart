part of 'text_align_bloc.dart';

abstract class TextAlignEvent extends Equatable {
  const TextAlignEvent();

  @override
  List<Object> get props => [];
}

class TextAlignRtlEvent extends TextAlignEvent {}

class TextAlignLtrEvent extends TextAlignEvent {}
