import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'text_align_event.dart';
part 'text_align_state.dart';

class TextAlignBloc extends Bloc<TextAlignEvent, TextAlignState> {
  TextAlignBloc() : super(TextAlignRtlState()) {
    on<TextAlignRtlEvent>((event, emit) {
      emit(TextAlignRtlState());
    });
    on<TextAlignLtrEvent>((event, emit) {
      emit(TextAlignLtrState());
    });
  }
}
