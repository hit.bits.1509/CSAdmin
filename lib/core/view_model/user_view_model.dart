import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/models/user_model.dart';
import 'package:csadmin/core/utilities/constants.dart';

class UserViewModel {
  final CollectionReference _usersCollectionRef =
      FireBaseConstants.instance.collection(FireBaseConstants.usersCollection);
  List<UserModel> get usermodel => _usermodel;
  List<UserModel> _usermodel = [];
  UserViewModel() {
    getusers();
  }

  getusers() async {
    _usersCollectionRef.get().then((value) {
      for (var i = 0; i < value.docs.length; i++) {
        _usersCollectionRef.add(
            UserModel.fromJson(value.docs[i].data() as Map<dynamic, dynamic>));
      }
    });
  }
}
