import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

//time
Duration myDuration = const Duration(milliseconds: 500);

//colors
const Color kNiceBlueColor = Color(0xff27abfe);
const Color kLightBlueColor = Color(0xffebf7ff);
const Color kNiceGreenColor = Color(0xff65dc40);
const Color kGreenColor = Color(0xFF2BBF5D);
const Color kDarkGreenColor = Color(0xff41613d);
const Color kNiceYellowColor = Color(0xfffdbb3a);
const Color kBorderGreyColor = Color(0xff737474);

//paddings & margins
const double kPaddingCard = 3;
const double kMarginCard = 10;

//vars & lists
const List<String> subsecribtions = [
  'مجاني',
  'ثقافة',
  'لغات',
  'رياضيات',
];

//constants inside firestore database
class FireBaseConstants {
  //firebase instance
  static final instance = FirebaseFirestore.instance;
  //collections
  static const String subjectsCollection = 'subjects';
  static const String usersCollection = 'users';
  //question
  static const String questionTag = 'question';
  static const String qText = 'questionTexe';
  static const String qAnswerText = 'answerTexe';
  static const String qBoolAnswer = 'isTrue';
  static const String qAnswerA = 'a';
  static const String qAnswerB = 'b';
  static const String qAnswerC = 'c';
  static const String qAnswerD = 'd';
  //users
  static const String userUsername = 'username';
  static const String userFirstName = 'firstName';
  static const String userLastName = 'lastName';
  static const String userNumber = 'number';
  static const String userStatus = 'status';
  //temp
  static const String temp = 'temp';
}
