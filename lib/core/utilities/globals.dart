// ignore_for_file: implementation_imports
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/blocs/text_align_bloc/bloc/text_align_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/src/provider.dart';

bool shouldPop = true;
AppBarBloc? usersAppBarBloc;
AppBarBloc? subjectsAppBarBloc;
AppBarBloc? depsAppBarBloc;
TextAlignBloc textAlignBloc = TextAlignBloc();

toggleAppBer(BuildContext context) {
  if (context.read<AppBarBloc>().state is AppBarMenuState) {
    context.read<AppBarBloc>().add(AppBarMainEvent());
    shouldPop = true;
  } else if (context.read<AppBarBloc>().state is AppBarMainState) {
    context.read<AppBarBloc>().add(AppBarMenuEvent());
    shouldPop = false;
  }
}
