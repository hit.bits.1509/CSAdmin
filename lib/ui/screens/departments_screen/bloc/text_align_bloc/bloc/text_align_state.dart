part of 'text_align_bloc.dart';

abstract class TextAlignState extends Equatable {
  const TextAlignState();

  @override
  List<Object> get props => [];
}

class TextAlignRtlState extends TextAlignState {}

class TextAlignLtrState extends TextAlignState {}
