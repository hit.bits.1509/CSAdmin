import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/screens/question_screen/questions_list.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/dynamic_app_bar.dart';
import 'package:csadmin/ui/widgets/loading_indicator.dart';
import 'package:csadmin/ui/widgets/my_alert_dailog.dart';
import 'package:csadmin/ui/widgets/my_card.dart';
import 'package:csadmin/ui/widgets/my_fab.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_toggle_appbar_detector.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';

import 'add_dept.dart';

class DepartmentScreen extends StatefulWidget {
  final String subjectName;
  const DepartmentScreen({required this.subjectName});

  @override
  State<DepartmentScreen> createState() => _DepartmentScreenState();
}

class _DepartmentScreenState extends State<DepartmentScreen> {
  late int selectedIndex;
  late BuildContext thisContext;
  String department = ' ';

  @override
  void initState() {
    super.initState();
    depsAppBarBloc = AppBarBloc();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppBarBloc>(
      create: (context) => depsAppBarBloc!,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: DynamicAppBar(
          title: 'أقسـام الـ' + widget.subjectName,
          appBarBloc: depsAppBarBloc!,
          appBarAction: AppBarAction.textAlign,
          onCopy: () {
            Clipboard.setData(
              ClipboardData(text: department + ' - ${widget.subjectName}'),
            );
            toggleAppBer(thisContext);
            ScaffoldMessenger.of(context).showSnackBar(
              MySnackBar(
                message: 'تم نسخ معلومات إلى الحافظة',
                icon: FontAwesomeIcons.clipboardCheck,
              ),
            );
          },
          onDelete: () {
            showDialog(
              context: context,
              builder: (context) => MyAlertDialog(
                message:
                    'هل انت متاكد من حذف $department من مادة ال${widget.subjectName}؟',
                onYes: () {
                  FireBaseConstants.instance
                      .collection(FireBaseConstants.subjectsCollection)
                      .doc(widget.subjectName)
                      .collection(widget.subjectName)
                      .doc(department)
                      .delete();
                  Navigator.pop(context);
                  toggleAppBer(thisContext);
                  ScaffoldMessenger.of(context).showSnackBar(
                    MySnackBar(message: 'تم حذف القسم'),
                  );
                },
                onNo: () {
                  Navigator.pop(context);
                },
              ),
            );
          },
        ),
        body: MyToggleAppBarDetector(
          context: context,
          bloc: depsAppBarBloc,
          child: WillPopScope(
            onWillPop: () async {
              if (!shouldPop) {
                toggleAppBer(thisContext);
                return false;
              } else {
                return shouldPop;
              }
            },
            child: SafeArea(
              child: StreamBuilder<QuerySnapshot>(
                stream: FireBaseConstants.instance
                    .collection(FireBaseConstants.subjectsCollection)
                    .doc(widget.subjectName)
                    .collection(widget.subjectName)
                    .snapshots(),
                builder: (context, snapshot) {
                  thisContext = context;
                  if (snapshot.hasData) {
                    final data = snapshot.requireData;
                    return GridView.builder(
                      padding: const EdgeInsets.all(10),
                      itemCount: data.size,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisSpacing: kPaddingCard,
                        mainAxisSpacing: kPaddingCard,
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (context, index) {
                        return BlocBuilder<AppBarBloc, AppBarState>(
                          builder: (context, state) {
                            final String dept = data.docs[index].id;
                            return Hero(
                              tag: 'card$index',
                              child: MyCard(
                                backgroundColor: state is AppBarMenuState &&
                                        (index == selectedIndex)
                                    ? kNiceBlueColor.withOpacity(0.4)
                                    : kLightBlueColor,
                                onLongTap: () {
                                  selectedIndex = index;
                                  department = dept;
                                  toggleAppBer(context);
                                },
                                onTap: shouldPop
                                    ? () async {
                                        selectedIndex = index;
                                        Navigator.push(
                                          context,
                                          PageTransition(
                                            child: QuestionsList(
                                              subjectName: widget.subjectName,
                                              department: dept,
                                            ),
                                            type:
                                                PageTransitionType.rightToLeft,
                                            duration: myDuration,
                                          ),
                                        );
                                        await FireBaseConstants.instance
                                            .collection(FireBaseConstants
                                                .subjectsCollection)
                                            .doc(widget.subjectName)
                                            .collection(widget.subjectName)
                                            .doc(dept)
                                            .update({
                                          FireBaseConstants.temp:
                                              FieldValue.delete()
                                        });
                                      }
                                    : () {
                                        toggleAppBer(context);
                                      },
                                iconData: FontAwesomeIcons.fileAlt,
                                text1: dept,
                              ),
                            );
                          },
                        );
                      },
                    );
                  } else if (snapshot.hasError) {
                    return const Center(child: Text('Error'));
                  }
                  return const Center(child: LoadingIndicator());
                },
              ),
            ),
          ),
        ),
        floatingActionButton: MyFAB(
          onPressed: () {
            if (depsAppBarBloc!.state is AppBarMenuState) {
              toggleAppBer(thisContext);
            }
            showModalBottomSheet(
              context: context,
              isScrollControlled: true,
              builder: (context) => AddDept(subjectName: widget.subjectName),
            );
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    depsAppBarBloc!.close();
  }
}
