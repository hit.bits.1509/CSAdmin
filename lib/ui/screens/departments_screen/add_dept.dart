import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/ui/widgets/my_bottom_sheet.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_text_field.dart';
import 'package:flutter/material.dart';

class AddDept extends StatefulWidget {
  final String subjectName;
  const AddDept({required this.subjectName});

  @override
  State<AddDept> createState() => _AddDeptState();
}

class _AddDeptState extends State<AddDept> {
  late String department;

  @override
  Widget build(BuildContext context) {
    return MyBottomSheet(
      title: 'إضافة قسم',
      onButtonPressed: () async {
        if (department != ' ') {
          ScaffoldMessenger.of(context).showSnackBar(
            MySnackBar(message: 'تم إضافة القسم بنجاح'),
          );
          Navigator.pop(context);
          await FireBaseConstants.instance
              .collection(FireBaseConstants.subjectsCollection)
              .doc(widget.subjectName)
              .collection(widget.subjectName)
              .doc(department)
              .set({FireBaseConstants.temp: 'temp'});
        }
      },
      child: MyTextField(
        hint: '...اسم القسم',
        controller: TextEditingController(),
        autofocus: true,
        onChanged: (newText) {
          department = newText;
        },
      ),
    );
  }
}
