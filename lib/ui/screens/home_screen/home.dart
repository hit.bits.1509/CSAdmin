import 'package:csadmin/ui/screens/home_screen/bloc/tab_bar_bloc.dart';
import 'package:csadmin/ui/screens/home_screen/dashboard.dart';
import 'package:csadmin/ui/screens/home_screen/manage.dart';
import 'package:csadmin/ui/widgets/app_bars/my_app_bar.dart';
import 'package:csadmin/ui/widgets/my_tab_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage();

  @override
  State<HomePage> createState() => _HomePageState();
}

var controller = PageController(initialPage: 0, keepPage: true);

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (val) {
        if (val.primaryVelocity! > 0) {
          context.read<TabBarBloc>().add(DashBoardEvent());
        } else if (val.primaryVelocity! < 0) {
          context.read<TabBarBloc>().add(ManageEvent());
        }
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: MyAppBar(
          title: 'HitBitz Admin',
          canPop: false,
        ),
        body: Column(
          children: [
            MyTabBar(controller: controller),
            Expanded(
              child: BlocBuilder<TabBarBloc, TabBarState>(
                builder: (context, state) {
                  return PageView(
                    controller: controller,
                    onPageChanged: (value) {
                      if (state is DashBoardState) {
                        context.read<TabBarBloc>().add(ManageEvent());
                      } else if (state is ManageState) {
                        context.read<TabBarBloc>().add(DashBoardEvent());
                      }
                    },
                    children: [
                      Dashboard(),
                      Manage(),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
