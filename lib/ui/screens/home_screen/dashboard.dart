import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/widgets/my_card.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () async {
          if (!shouldPop) {
            // toggleAppBer(thisContext);
            return false;
          } else {
            return shouldPop;
          }
        },
        child: Container(
          margin: const EdgeInsets.all(kMarginCard),
          child: GridView(
            physics: const ScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              mainAxisSpacing: kPaddingCard,
              crossAxisSpacing: kPaddingCard,
              crossAxisCount: 2,
            ),
            children: [
              Hero(
                tag: 'card0',
                child: MyCard(
                  text1: 'فارغ',
                  onTap: () {},
                ),
              ),
              Hero(
                tag: 'card1',
                child: MyCard(
                  text1: 'فارغ',
                  onTap: () {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
