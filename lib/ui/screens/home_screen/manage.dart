import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/screens/subjects_screen/subjects_screen.dart';
import 'package:csadmin/ui/screens/users_screen/users_screen.dart';
import 'package:csadmin/ui/widgets/my_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';

class Manage extends StatelessWidget {
  const Manage();

  @override
  Widget build(BuildContext context) {
    subjectsAppBarBloc = AppBarBloc();
    usersAppBarBloc = AppBarBloc();
    return Scaffold(
      backgroundColor: Colors.white,
      body: WillPopScope(
        onWillPop: () async {
          if (!shouldPop) {
            // toggleAppBer(thisContext);
            return false;
          } else {
            return shouldPop;
          }
        },
        child: Container(
          margin: const EdgeInsets.all(kMarginCard),
          child: GridView(
            physics: const ScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              mainAxisSpacing: kPaddingCard,
              crossAxisSpacing: kPaddingCard,
              crossAxisCount: 2,
            ),
            children: [
              Hero(
                tag: 'card0',
                child: MyCard(
                  iconData: FontAwesomeIcons.bookOpen,
                  text1: 'المواد',
                  onTap: () {
                    Navigator.push(
                      context,
                      PageTransition(
                        child: BlocProvider<AppBarBloc>(
                          create: (appBarBlocContext) => subjectsAppBarBloc!,
                          child: const SubjectsScreen(),
                        ),
                        type: PageTransitionType.rightToLeft,
                        duration: myDuration,
                      ),
                    );
                  },
                ),
              ),
              Hero(
                tag: 'card1',
                child: MyCard(
                  iconData: FontAwesomeIcons.usersCog,
                  text1: 'المستخدمين',
                  onTap: () {
                    // Navigator.pop(context);
                    Navigator.push(
                      context,
                      PageTransition(
                        child: BlocProvider<AppBarBloc>(
                          create: (appBarBlocContext) => usersAppBarBloc!,
                          child: UsersScreen(),
                        ),
                        type: PageTransitionType.rightToLeft,
                        duration: myDuration,
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
