part of 'tab_bar_bloc.dart';

abstract class TabBarEvent extends Equatable {
  const TabBarEvent();

  @override
  List<Object> get props => [];
}

class DashBoardEvent extends TabBarEvent {}

class ManageEvent extends TabBarEvent {}
