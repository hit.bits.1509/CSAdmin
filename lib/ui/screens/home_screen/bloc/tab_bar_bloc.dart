import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'tab_bar_event.dart';
part 'tab_bar_state.dart';

class TabBarBloc extends Bloc<TabBarEvent, TabBarState> {
  TabBarBloc() : super(DashBoardState()) {
    on<DashBoardEvent>((event, emit) => emit(DashBoardState()));
    on<ManageEvent>((event, emit) => emit(ManageState()));
  }
}
