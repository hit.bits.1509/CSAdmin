part of 'tab_bar_bloc.dart';

abstract class TabBarState extends Equatable {
  const TabBarState();
  
  @override
  List<Object> get props => [];
}

class TabBarInitial extends TabBarState {}

class DashBoardState extends TabBarState {}

class ManageState extends TabBarState {}

