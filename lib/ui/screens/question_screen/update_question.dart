import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/blocs/text_align_bloc/bloc/text_align_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/core/models/choice_model.dart';
import 'package:csadmin/core/models/question_model.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/my_app_bar.dart';
import 'package:csadmin/ui/widgets/my_button.dart';
import 'package:csadmin/ui/widgets/my_choice_tile.dart';
import 'package:csadmin/ui/widgets/my_pop_detector.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_text_area.dart';
import 'package:csadmin/ui/screens/question_screen/bloc/checkbox_cubit_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UpdateQuestion extends StatefulWidget {
  final String subjectName;
  final String department;
  final String id;
  final QuerySnapshot data;
  final int index;

  const UpdateQuestion({
    required this.id,
    required this.subjectName,
    required this.department,
    required this.data,
    required this.index,
  });

  @override
  State<UpdateQuestion> createState() => _UpdateQuestionState();
}

class _UpdateQuestionState extends State<UpdateQuestion> {
  late String subject = widget.subjectName;
  late String dept = widget.department;
  late QuerySnapshot data = widget.data;
  late Question question;

  @override
  void initState() {
    super.initState();
    question = Question(
      subject: '',
      text: data.docs[widget.index].get(FireBaseConstants.questionTag),
      department: ' ',
      choices: [
        Choice(
            isTrue: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerA][FireBaseConstants.qBoolAnswer],
            text: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerA][FireBaseConstants.qAnswerText]),
        Choice(
            isTrue: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerB][FireBaseConstants.qBoolAnswer],
            text: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerB][FireBaseConstants.qAnswerText]),
        Choice(
            isTrue: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerC][FireBaseConstants.qBoolAnswer],
            text: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerC][FireBaseConstants.qAnswerText]),
        Choice(
            isTrue: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerD][FireBaseConstants.qBoolAnswer],
            text: jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                FireBaseConstants.qAnswerD][FireBaseConstants.qAnswerText]),
      ],
    );
    question.subject = widget.subjectName;
    question.department = widget.department;
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider.value(
          value: textAlignBloc,
        ),
        BlocProvider<CheckboxCubit>(
          create: (context) => CheckboxCubit(),
        )
      ],
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: MyAppBar(
          title: 'تعديل السؤال',
          action: AppBarAction.textAlign,
          canPop: true,
        ),
        body: MyPopDetector(
          child: SafeArea(
            child: ListView(
              padding: const EdgeInsets.all(kMarginCard),
              children: [
                const SizedBox(height: 10),
                Center(
                  child: Text(
                    '${widget.subjectName} - ${widget.department}',
                    style: const TextStyle(
                      color: kNiceYellowColor,
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: BlocBuilder<TextAlignBloc, TextAlignState>(
                    builder: (context, state) {
                      return MyTextArea(
                        textAlign: (state is TextAlignRtlState)
                            ? TextAlign.right
                            : TextAlign.left,
                        onChanged: (val) {
                          question.text = val;
                        },
                        lines: 5,
                        hint: '\n\nالــسؤال',
                        // color: Colors.white,
                        controller: TextEditingController(
                          text: data.docs[widget.index]
                              .get(FireBaseConstants.questionTag),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(height: 20),
                BlocBuilder<TextAlignBloc, TextAlignState>(
                  builder: (context, state) {
                    return MyChoiceTile(
                      question: question,
                      index: 0,
                      letter: 'A',
                      textAlign: (state is TextAlignRtlState)
                          ? TextAlign.right
                          : TextAlign.left,
                      controller: TextEditingController(
                        text: jsonDecode(
                                    jsonEncode(data.docs[widget.index].data()))[
                                FireBaseConstants
                                    .qAnswerA][FireBaseConstants.qAnswerText]
                            .toString(),
                      ),
                    );
                  },
                ),
                BlocBuilder<TextAlignBloc, TextAlignState>(
                  builder: (context, state) {
                    return MyChoiceTile(
                      question: question,
                      index: 1,
                      letter: 'B',
                      textAlign: (state is TextAlignRtlState)
                          ? TextAlign.right
                          : TextAlign.left,
                      controller: TextEditingController(
                        text: jsonDecode(
                                    jsonEncode(data.docs[widget.index].data()))[
                                FireBaseConstants
                                    .qAnswerB][FireBaseConstants.qAnswerText]
                            .toString(),
                      ),
                    );
                  },
                ),
                if (jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                                FireBaseConstants.qAnswerC]
                            [FireBaseConstants.qAnswerText]
                        .toString() !=
                    ' ')
                  BlocBuilder<TextAlignBloc, TextAlignState>(
                    builder: (context, state) {
                      return MyChoiceTile(
                        question: question,
                        index: 2,
                        letter: 'C',
                        textAlign: (state is TextAlignRtlState)
                            ? TextAlign.right
                            : TextAlign.left,
                        controller: TextEditingController(
                          text: jsonDecode(jsonEncode(data.docs[widget.index]
                                      .data()))[FireBaseConstants.qAnswerC]
                                  [FireBaseConstants.qAnswerText]
                              .toString(),
                        ),
                      );
                    },
                  ),
                if (jsonDecode(jsonEncode(data.docs[widget.index].data()))[
                                FireBaseConstants.qAnswerD]
                            [FireBaseConstants.qAnswerText]
                        .toString() !=
                    ' ')
                  BlocBuilder<TextAlignBloc, TextAlignState>(
                    builder: (context, state) {
                      return MyChoiceTile(
                        question: question,
                        index: 3,
                        letter: 'D',
                        textAlign: (state is TextAlignRtlState)
                            ? TextAlign.right
                            : TextAlign.left,
                        controller: TextEditingController(
                          text: jsonDecode(jsonEncode(data.docs[widget.index]
                                      .data()))[FireBaseConstants.qAnswerD]
                                  [FireBaseConstants.qAnswerText]
                              .toString(),
                        ),
                      );
                    },
                  ),
                const SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: MyButton(
                    text: 'تحديث',
                    onPressed: () {
                      FireBaseConstants.instance
                          .collection(FireBaseConstants.subjectsCollection)
                          .doc(widget.subjectName)
                          .collection(widget.subjectName)
                          .doc(widget.department)
                          .collection(widget.department)
                          .doc(widget.id)
                          .set({
                        FireBaseConstants.questionTag: question.text,
                        FireBaseConstants.qAnswerA: {
                          FireBaseConstants.qAnswerText:
                              question.choices[0].text,
                          FireBaseConstants.qBoolAnswer:
                              question.choices[0].isTrue
                        },
                        FireBaseConstants.qAnswerB: {
                          FireBaseConstants.qAnswerText:
                              question.choices[1].text,
                          FireBaseConstants.qBoolAnswer:
                              question.choices[1].isTrue
                        },
                        FireBaseConstants.qAnswerC: {
                          FireBaseConstants.qAnswerText:
                              question.choices[2].text,
                          FireBaseConstants.qBoolAnswer:
                              question.choices[2].isTrue
                        },
                        FireBaseConstants.qAnswerD: {
                          FireBaseConstants.qAnswerText:
                              question.choices[3].text,
                          FireBaseConstants.qBoolAnswer:
                              question.choices[3].isTrue
                        },
                      });
                      Navigator.pop(context);
                      ScaffoldMessenger.of(context).showSnackBar(
                        MySnackBar(message: 'تم تعديل السؤال بنجاح'),
                      );
                    },
                    color: kNiceBlueColor,
                    textColor: Colors.white,
                    isPressed: true,
                    elevation: 5,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
