import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/blocs/text_align_bloc/bloc/text_align_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/my_app_bar.dart';
import 'package:csadmin/ui/widgets/loading_indicator.dart';
import 'package:csadmin/ui/widgets/my_alert_dailog.dart';
import 'package:csadmin/ui/widgets/my_fab.dart';
import 'package:csadmin/ui/widgets/my_list_tile.dart';
import 'package:csadmin/ui/widgets/my_pop_detector.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';
import 'add_question.dart';
import 'update_question.dart';

class QuestionsList extends StatelessWidget {
  final String department;
  final String subjectName;
  QuestionsList({
    required this.subjectName,
    required this.department,
  });

  @override
  Widget build(BuildContext context) {
    return BlocProvider.value(
      value: textAlignBloc,
      child: MyPopDetector(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: MyAppBar(
            title: department,
            canPop: true,
            action: AppBarAction.textAlign,
          ),
          body: SafeArea(
            child: StreamBuilder<QuerySnapshot>(
              stream: FireBaseConstants.instance
                  .collection(FireBaseConstants.subjectsCollection)
                  .doc(subjectName)
                  .collection(subjectName)
                  .doc(department)
                  .collection(department)
                  .snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  final data = snapshot.requireData;
                  if (data.size == 0) {
                    return const Center(
                      child: Text(
                        'انقر زر الإضافة + لاضافة السؤال الاول',
                        style: TextStyle(
                          color: kBorderGreyColor,
                        ),
                      ),
                    );
                  }
                  return ListView.builder(
                    itemCount: data.size + 1,
                    itemBuilder: (context, index) {
                      return (index == data.size)
                          ? Container(
                              margin: const EdgeInsets.all(kMarginCard),
                              color: Colors.white,
                            )
                          : MyListTile(
                              headIconButton: IconButton(
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    PageTransition(
                                      child: UpdateQuestion(
                                        id: data.docs[index].id,
                                        department: department,
                                        subjectName: subjectName,
                                        data: data,
                                        index: index,
                                      ),
                                      type: PageTransitionType.rightToLeft,
                                    ),
                                  );
                                },
                                icon: const FaIcon(
                                  FontAwesomeIcons.pencilAlt,
                                  color: kNiceBlueColor,
                                ),
                              ),
                              title: BlocBuilder<TextAlignBloc, TextAlignState>(
                                builder: (context, state) {
                                  return Text(
                                    data.docs[index]
                                        .get(FireBaseConstants.questionTag),
                                    style: const TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    textAlign: (state is TextAlignRtlState)
                                        ? TextAlign.right
                                        : TextAlign.left,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  );
                                },
                              ),
                              subTitle:
                                  BlocBuilder<TextAlignBloc, TextAlignState>(
                                builder: (context, state) {
                                  return Text(
                                    subjectName +
                                        ' - ' +
                                        department,
                                    textAlign: (state is TextAlignRtlState)
                                        ? TextAlign.right
                                        : TextAlign.left,
                                  );
                                },
                              ),
                              tileColor: kLightBlueColor,
                              tailIconButton: IconButton(
                                icon: const FaIcon(
                                  FontAwesomeIcons.trashAlt,
                                  color: Color(0xffd65252), //Colors.redAccent,
                                ),
                                onPressed: () {
                                  showDialog(
                                    context: context,
                                    builder: (context) => MyAlertDialog(
                                      message:
                                          'هل أنت متأكد أنك تريد حذف السؤال؟',
                                      onYes: () {
                                        FireBaseConstants.instance
                                            .collection(FireBaseConstants
                                                .subjectsCollection)
                                            .doc(subjectName)
                                            .collection(subjectName)
                                            .doc(department)
                                            .collection(department)
                                            .doc(data.docs[index].id)
                                            .delete();
                                        Navigator.pop(context);
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          MySnackBar(message: 'تم حذف السؤال'),
                                        );
                                      },
                                      onNo: () {
                                        Navigator.pop(context);
                                      },
                                    ),
                                  );
                                },
                              ),
                            );
                    },
                  );
                } else if (snapshot.hasError) {
                  return const Center(child: Text('Error'));
                }
                return const Center(child: LoadingIndicator());
              },
            ),
          ),
          floatingActionButton: MyFAB(
            onPressed: () {
              Navigator.push(
                context,
                PageTransition(
                  child: AddQuestion(
                    subjectName: subjectName,
                    department: department,
                  ),
                  type: PageTransitionType.rightToLeft,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
