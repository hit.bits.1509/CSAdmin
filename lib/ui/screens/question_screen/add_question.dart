import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/core/models/choice_model.dart';
import 'package:csadmin/core/models/question_model.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/my_app_bar.dart';
import 'package:csadmin/ui/widgets/my_button.dart';
import 'package:csadmin/ui/widgets/my_choice_tile.dart';
import 'package:csadmin/ui/widgets/my_pop_detector.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_text_area.dart';
import 'package:csadmin/ui/screens/question_screen/bloc/checkbox_cubit_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AddQuestion extends StatefulWidget {
  final String subjectName;
  final String department;

  const AddQuestion({
    Key? key,
    required this.subjectName,
    required this.department,
  }) : super(key: key);

  @override
  State<AddQuestion> createState() => _AddQuestionState();
}

class _AddQuestionState extends State<AddQuestion> {
  late Question question = Question(
    subject: ' ',
    department: ' ',
    text: ' ',
    choices: [
      Choice(isTrue: false, text: ' '),
      Choice(isTrue: false, text: ' '),
      Choice(isTrue: false, text: ' '),
      Choice(isTrue: false, text: ' '),
    ],
  );

  @override
  void initState() {
    super.initState();
    question.subject = widget.subjectName;
    question.department = widget.department;
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CheckboxCubit(),
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: MyAppBar(
          title: 'إضافة سؤال',
          action: AppBarAction.textAlign,
          canPop: true,
        ),
        body: MyPopDetector(
          child: SafeArea(
            child: ListView(
              padding: const EdgeInsets.all(kMarginCard),
              children: [
                const SizedBox(height: 10),
                Center(
                  child: Text(
                    '${widget.subjectName} - ${widget.department}',
                    style: const TextStyle(
                      color: kNiceYellowColor,
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: MyTextArea(
                    onChanged: (val) {
                      question.text = val;
                    },
                    lines: 5,
                    hint: '\n\nالــسؤال',
                    controller: TextEditingController(),
                  ),
                ),
                const SizedBox(height: 20),
                MyChoiceTile(
                  question: question,
                  index: 0,
                  letter: 'A',
                  controller: TextEditingController(),
                ),
                MyChoiceTile(
                  question: question,
                  index: 1,
                  letter: 'B',
                  controller: TextEditingController(),
                ),
                MyChoiceTile(
                  question: question,
                  index: 2,
                  letter: 'C',
                  controller: TextEditingController(),
                ),
                MyChoiceTile(
                  question: question,
                  index: 3,
                  letter: 'D',
                  controller: TextEditingController(),
                ),
                const SizedBox(height: 20),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: MyButton(
                    // width: double.infinity,
                    text: 'إضــافــة',
                    color: kNiceBlueColor,
                    textColor: Colors.white,
                    isPressed: true,
                    elevation: 5,
                    onPressed: () {
                      if (question.text != ' ' &&
                          question.choices[0].text != ' ' &&
                          question.choices[1].text != ' ') {
                        FireBaseConstants.instance
                            .collection(FireBaseConstants.subjectsCollection)
                            .doc(widget.subjectName)
                            .collection(widget.subjectName)
                            .doc(widget.department)
                            .collection(widget.department)
                            .add({
                          FireBaseConstants.questionTag: question.text,
                          FireBaseConstants.qAnswerA: {
                            FireBaseConstants.qAnswerText:
                                question.choices[0].text,
                            FireBaseConstants.qBoolAnswer:
                                question.choices[0].isTrue
                          },
                          FireBaseConstants.qAnswerB: {
                            FireBaseConstants.qAnswerText:
                                question.choices[1].text,
                            FireBaseConstants.qBoolAnswer:
                                question.choices[1].isTrue
                          },
                          FireBaseConstants.qAnswerC: {
                            FireBaseConstants.qAnswerText:
                                question.choices[2].text,
                            FireBaseConstants.qBoolAnswer:
                                question.choices[2].isTrue
                          },
                          FireBaseConstants.qAnswerD: {
                            FireBaseConstants.qAnswerText:
                                question.choices[3].text,
                            FireBaseConstants.qBoolAnswer:
                                question.choices[3].isTrue
                          }
                        });
                        Navigator.pop(context);
                        ScaffoldMessenger.of(context).showSnackBar(
                          MySnackBar(message: 'تم إضافة السؤال بنجاح'),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
