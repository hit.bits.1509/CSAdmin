part of 'checkbox_cubit_cubit.dart';

class CheckboxState {
  final bool choiceA, choiceB, choiceC, choiceD;
  CheckboxState({
    required this.choiceA,
    required this.choiceB,
    required this.choiceC,
    required this.choiceD,
  });

  CheckboxState copywithA({required bool checkbox}) {
    return CheckboxState(
      choiceA: checkbox,
      choiceB: choiceB,
      choiceC: choiceC,
      choiceD: choiceD,
    );
  }

  CheckboxState copywithB({required bool checkbox}) {
    return CheckboxState(
      choiceA: choiceA,
      choiceB: checkbox,
      choiceC: choiceC,
      choiceD: choiceD,
    );
  }

  CheckboxState copywithC({required bool checkbox}) {
    return CheckboxState(
      choiceA: choiceA,
      choiceB: choiceB,
      choiceC: checkbox,
      choiceD: choiceD,
    );
  }

  CheckboxState copywithD({required bool checkbox}) {
    return CheckboxState(
      choiceA: choiceA,
      choiceB: choiceB,
      choiceC: choiceC,
      choiceD: checkbox,
    );
  }
}
