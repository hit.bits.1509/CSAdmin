import 'package:bloc/bloc.dart';
part 'checkbox_cubit_state.dart';

class CheckboxCubit extends Cubit<CheckboxState> {
  CheckboxCubit()
      : super(
          CheckboxState(
            choiceA: false,
            choiceB: false,
            choiceC: false,
            choiceD: false,
          ),
        );

  void toggleChecboxValue(bool newvlaue) {
    emit(state.copywithA(checkbox: newvlaue));
  }
}
