part of 'show_caution_bloc.dart';

abstract class ShowCautionEvent extends Equatable {
  const ShowCautionEvent();

  @override
  List<Object> get props => [];
}


class ShowCautionFalseEvent extends ShowCautionEvent {}

class ShowCautionTrueEvent extends ShowCautionEvent {}
