import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'show_caution_event.dart';
part 'show_caution_state.dart';

class ShowCautionBloc extends Bloc<ShowCautionEvent, ShowCautionState> {
  ShowCautionBloc() : super(ShowCautionFalseState()) {
    on<ShowCautionFalseEvent>((event, emit) {
      emit(ShowCautionFalseState());
    });
    on<ShowCautionTrueEvent>((event, emit) {
      emit(ShowCautionTrueState());
    });
  }
}
