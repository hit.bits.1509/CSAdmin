part of 'show_caution_bloc.dart';

abstract class ShowCautionState extends Equatable {
  const ShowCautionState();

  @override
  List<Object> get props => [];
}

class ShowCautionFalseState extends ShowCautionState {}

class ShowCautionTrueState extends ShowCautionState {}
