import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/models/user_model.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/screens/users_screen/users_screen.dart'
    as users_screen;
import 'package:csadmin/ui/widgets/loading_indicator.dart';
import 'package:csadmin/ui/widgets/my_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

// ignore: must_be_immutable
class SearchResultsListView extends StatefulWidget {
  final String? searchTerm;

  SearchResultsListView({required this.searchTerm}) {
    if (this.searchTerm != null)
      users_screen.searchResult![0] = this.searchTerm!.toLowerCase();
  }

  @override
  State<SearchResultsListView> createState() => _SearchResultsListViewState();
}

class _SearchResultsListViewState extends State<SearchResultsListView> {
  int selectedIndex = -1;

  List<String>? searchResultEnd;

  @override
  void initState() {
    super.initState();
    usersAppBarBloc = AppBarBloc();
  }

  @override
  void dispose() {
    super.dispose();
    usersAppBarBloc!.close();
  }

  @override
  Widget build(BuildContext context) {
    if (users_screen.searchResult != null) {
      searchResultEnd = [
        (users_screen.searchResult![0].toLowerCase() + '\uf8ff').toLowerCase()
      ];
    }
    return Padding(
      padding: EdgeInsets.only(top: 0),
      child: StreamBuilder<QuerySnapshot>(
        stream: users_screen.searchResult == null
            ? FireBaseConstants.instance
                .collection(FireBaseConstants.usersCollection)
                .orderBy(FireBaseConstants.userFirstName)
                .snapshots()
            : FireBaseConstants.instance
                .collection(FireBaseConstants.usersCollection)
                .orderBy(FireBaseConstants.userFirstName)
                .startAt(users_screen.searchResult!)
                .endAt(searchResultEnd!)
                .snapshots(),
        builder: (builderContext, snapshot) {
          if (snapshot.hasData) {
            users_screen.data = snapshot.requireData;
            return GridView.builder(
              padding: const EdgeInsets.all(kMarginCard),
              itemCount: users_screen.data.size,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisSpacing: kPaddingCard,
                mainAxisSpacing: kPaddingCard,
                crossAxisCount: 2,
              ),
              itemBuilder: (context, index) {
                String username = users_screen.data.docs[index]
                    .get(FireBaseConstants.userUsername);
                String firstName = users_screen.data.docs[index]
                    .get(FireBaseConstants.userFirstName);
                String lastName = users_screen.data.docs[index]
                    .get(FireBaseConstants.userLastName);
                String number = users_screen.data.docs[index]
                    .get(FireBaseConstants.userNumber);
                String status = users_screen.data.docs[index]
                    .get(FireBaseConstants.userStatus);
                users_screen.user = UserModel(
                  username: username,
                  firstName: firstName,
                  lastName: lastName,
                  number: number,
                  status: status,
                );
                return BlocBuilder<AppBarBloc, AppBarState>(
                  builder: (context, state) {
                    return Hero(
                      tag: 'card$index',
                      child: MyCard(
                        backgroundColor:
                            state is AppBarMenuState && (index == selectedIndex)
                                ? kNiceBlueColor.withOpacity(0.4)
                                : kLightBlueColor,
                        iconData: FontAwesomeIcons.userEdit,
                        lines: 4,
                        text1: firstName + ' ' + lastName,
                        text2: username,
                        text3: number,
                        text4: status,
                        onTap: () {
                          if (usersAppBarBloc!.state is AppBarMenuState) {
                            toggleAppBer(context);
                          }
                        },
                        onLongTap: () {
                          username = users_screen.data.docs[index]
                              .get(FireBaseConstants.userUsername);
                          firstName = users_screen.data.docs[index]
                              .get(FireBaseConstants.userFirstName);
                          lastName = users_screen.data.docs[index]
                              .get(FireBaseConstants.userLastName);
                          number = users_screen.data.docs[index]
                              .get(FireBaseConstants.userNumber);
                          status = users_screen.data.docs[index]
                              .get(FireBaseConstants.userStatus);
                          users_screen.thisContext = context;
                          selectedIndex = index;
                          users_screen.userId =
                              users_screen.data.docs[selectedIndex].id;
                          users_screen.user = UserModel(
                            username: username,
                            firstName: firstName,
                            lastName: lastName,
                            number: number,
                            status: status,
                          );
                          context.read<AppBarBloc>().add(AppBarMenuEvent());
                          shouldPop = false;
                        },
                      ),
                    );
                  },
                );
              },
            );
          } else if (snapshot.hasError) {
            return const Center(child: Text('Error'));
          }
          return const Center(child: LoadingIndicator());
        },
      ),
    );
  }
}
