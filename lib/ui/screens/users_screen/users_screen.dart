// ignore_for_file: import_of_legacy_library_into_null_safe
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/models/user_model.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/screens/users_screen/add_edit_user.dart';
import 'package:csadmin/ui/screens/users_screen/bloc/bloc/show_caution_bloc.dart';
import 'package:csadmin/ui/screens/users_screen/search_result.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/dynamic_app_bar.dart';
import 'package:csadmin/ui/widgets/my_alert_dailog.dart';
import 'package:csadmin/ui/widgets/my_fab.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_toggle_appbar_detector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';
import 'package:page_transition/page_transition.dart';

List<String>? searchResult;
late dynamic data;
late BuildContext thisContext;
late UserModel user;
late String userId;

class UsersScreen extends StatefulWidget {
  @override
  _UsersScreenState createState() => _UsersScreenState();
}

class _UsersScreenState extends State<UsersScreen> {
  static const historyLength = 5;

  List<String> _searchHistory = [];

  late List<String> filteredSearchHistory;

  String? selectedTerm;

  List<String> filterSearchTerms({
    String? filter,
  }) {
    if (filter != null && filter.isNotEmpty) {
      return _searchHistory.reversed
          .where((term) => term.startsWith(filter))
          .toList();
    } else {
      return _searchHistory.reversed.toList();
    }
  }

  void addSearchTerm(String term) {
    if (_searchHistory.contains(term)) {
      putSearchTermFirst(term);
      return;
    }

    _searchHistory.add(term);
    if (_searchHistory.length > historyLength) {
      _searchHistory.removeRange(0, _searchHistory.length - historyLength);
    }

    filteredSearchHistory = filterSearchTerms(filter: null);
  }

  void deleteSearchTerm(String term) {
    _searchHistory.removeWhere((t) => t == term);
    filteredSearchHistory = filterSearchTerms(filter: null);
  }

  void putSearchTermFirst(String term) {
    deleteSearchTerm(term);
    addSearchTerm(term);
  }

  late FloatingSearchBarController controller;

  @override
  void initState() {
    super.initState();
    controller = FloatingSearchBarController();
    filteredSearchHistory = filterSearchTerms(filter: null);
    // showCautionBloc = ShowCautionBloc();
    usersAppBarBloc = AppBarBloc();
  }

  @override
  void dispose() {
    controller.dispose();
    usersAppBarBloc!.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: DynamicAppBar(
        title: 'المستخدمين',
        appBarBloc: usersAppBarBloc!,
        appBarAction: AppBarAction.refreshPage,
        onActionPressed: () {
          //TODO: BLoC
          setState(() {
            searchResult = null;
            selectedTerm = null;
          });
          ScaffoldMessenger.of(context).showSnackBar(
            MySnackBar(message: 'Page refreshed'),
          );
        },
        onDelete: () {
          showDialog(
            context: context,
            builder: (context) {
              return MyAlertDialog(
                message:
                    'هل انت متاكد من حذف المستخدم ${user.firstName} ${user.lastName} نهائيا؟',
                onYes: () {
                  FireBaseConstants.instance
                      .collection(FireBaseConstants.usersCollection)
                      .doc(userId)
                      .delete();
                  toggleAppBer(thisContext);
                  Navigator.pop(context);
                  ScaffoldMessenger.of(context).showSnackBar(
                    MySnackBar(message: 'تم حذف المستخدم'),
                  );
                },
                onNo: () {
                  Navigator.pop(context);
                },
              );
            },
          );
        },
        onCopy: () {
          Clipboard.setData(
            ClipboardData(
              text: user.firstName +
                  ' ' +
                  user.lastName +
                  '\n' +
                  user.username +
                  '\n' +
                  user.number +
                  '\n' +
                  user.status!,
            ),
          );
          toggleAppBer(thisContext);
          ScaffoldMessenger.of(context).showSnackBar(
            MySnackBar(
              message: 'تم نسخ معلومات المستخدم إلى الحافظة',
              icon: FontAwesomeIcons.clipboardCheck,
            ),
          );
        },
        onUpdate: () {
          Navigator.push(
            context,
            PageTransition(
              child: BlocProvider<ShowCautionBloc>(
                create: (context) => ShowCautionBloc(),
                child: AddEditUser(userModel: user, userId: userId),
              ),
              type: PageTransitionType.rightToLeft,
              duration: myDuration,
            ),
          );
          Future.delayed(
            Duration(seconds: 1),
            () {
              if (usersAppBarBloc!.state is AppBarMenuState) {
                toggleAppBer(thisContext);
              }
            },
          );
        },
      ),
      body: WillPopScope(
        onWillPop: () async {
          if (!shouldPop) {
            toggleAppBer(thisContext);
            return false;
          } else {
            return shouldPop;
          }
        },
        child: MyToggleAppBarDetector(
          context: context,
          child: FloatingSearchBar(
            iconColor: kBorderGreyColor,
            accentColor: kBorderGreyColor,
            backgroundColor: kNiceYellowColor,
            // backgroundColor: kLightBlueColor,
            margins: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
            borderRadius: BorderRadius.circular(15),
            elevation: 5,
            height: 60,
            padding: EdgeInsets.symmetric(horizontal: 10),
            width: MediaQuery.of(context).size.width / 6,
            openWidth: MediaQuery.of(context).size.width,
            automaticallyImplyBackButton: false,
            controller: controller,
            physics: BouncingScrollPhysics(),
            transition: CircularFloatingSearchBarTransition(),
            hint: 'Search and find out...',
            title: Text(
              selectedTerm ?? 'Search for a user',
              style: TextStyle(
                  color: kBorderGreyColor,
                  fontSize: MediaQuery.of(context).size.height / 40),
            ),
            body: FloatingSearchBarScrollNotifier(
              child: SearchResultsListView(
                searchTerm: selectedTerm,
              ),
            ),
            actions: [
              FloatingSearchBarAction.searchToClear(),
            ],
            onQueryChanged: (query) {
              setState(() {
                filteredSearchHistory = filterSearchTerms(filter: query);
              });
            },
            onSubmitted: (query) {
              setState(() {
                addSearchTerm(query);
                selectedTerm = query;
                searchResult = [selectedTerm!];
              });
              controller.close();
            },
            builder: (context, transition) {
              return ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Material(
                  // color: Colors.white,
                  color: kNiceYellowColor,
                  elevation: 5,
                  child: Builder(
                    builder: (context) {
                      if (filteredSearchHistory.isEmpty &&
                          controller.query.isEmpty) {
                        return Container(
                          height: 56,
                          width: double.infinity,
                          alignment: Alignment.center,
                          child: Text(
                            'Start searching',
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: Theme.of(context).textTheme.caption,
                          ),
                        );
                      } else if (filteredSearchHistory.isEmpty) {
                        return ListTile(
                          title: Text(controller.query),
                          leading: const Icon(Icons.search),
                          onTap: () {
                            setState(() {
                              addSearchTerm(controller.query);
                              selectedTerm = controller.query;
                              searchResult = [selectedTerm!];
                            });
                            controller.close();
                          },
                        );
                      } else {
                        return Column(
                          mainAxisSize: MainAxisSize.min,
                          children: filteredSearchHistory
                              .map(
                                (term) => ListTile(
                                  title: Text(
                                    term,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                  leading: const Icon(Icons.history),
                                  trailing: IconButton(
                                    icon: const Icon(Icons.clear),
                                    onPressed: () {
                                      setState(() {
                                        deleteSearchTerm(term);
                                      });
                                    },
                                  ),
                                  onTap: () {
                                    setState(() {
                                      putSearchTermFirst(term);
                                      selectedTerm = term;
                                      searchResult = [selectedTerm!];
                                    });
                                    controller.close();
                                  },
                                ),
                              )
                              .toList(),
                        );
                      }
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: MyFAB(
        onPressed: () {
          Navigator.push(
            context,
            PageTransition(
              child: BlocProvider<ShowCautionBloc>(
                create: (context) => ShowCautionBloc(),
                child: AddEditUser(),
              ),
              type: PageTransitionType.rightToLeft,
              duration: myDuration,
            ),
          );
          Future.delayed(
            Duration(seconds: 1),
            () {
              if (usersAppBarBloc!.state is AppBarMenuState) {
                toggleAppBer(thisContext);
              }
              searchResult = null;
              setState(() {});
            },
          );
        },
      ),
    );
  }
}
