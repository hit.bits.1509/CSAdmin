import 'package:csadmin/core/models/user_model.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/ui/screens/users_screen/bloc/bloc/show_caution_bloc.dart';
import 'package:csadmin/ui/screens/users_screen/users_screen.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/app_bars/my_app_bar.dart';
import 'package:csadmin/ui/widgets/my_button.dart';
import 'package:csadmin/ui/widgets/my_check_box.dart';
import 'package:csadmin/ui/widgets/my_label.dart';
import 'package:csadmin/ui/widgets/my_pop_detector.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/rounded_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// ignore: must_be_immutable
class AddEditUser extends StatelessWidget {
  UserModel? userModel;
  String? userId;
  late final String pictureFun;

  AddEditUser({
    this.userModel,
    this.userId,
  }) {
    if (userModel == null) {
      pictureFun = 'add';
      userModel = UserModel(
        firstName: '',
        lastName: '',
        username: '',
        number: '',
      );
    } else {
      pictureFun = 'edit';
    }
  }

  bool showCaution = false;

  @override
  Widget build(BuildContext context) {
    return MyPopDetector(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: MyAppBar(
          title: (userId == null) ? 'إضافة مستخدم' : 'تعديل المستخدم',
          //TODO refresh

          // action: AppBarAction.refreshPage,
          canPop: true,
        ),
        body: SafeArea(
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 45,
                  vertical: 10,
                ),
                child: Image.asset(
                  'assets/images/$pictureFun-user.png',
                  frameBuilder: (
                    BuildContext context,
                    Widget child,
                    int? frame,
                    bool wasSynchronouslyLoaded,
                  ) {
                    if (wasSynchronouslyLoaded) {
                      return child;
                    }
                    return AnimatedSize(
                      alignment: Alignment.topCenter,
                      duration: const Duration(milliseconds: 500),
                      curve: Curves.ease,
                      child: child,
                    );
                  },
                ),
              ),
              MyLabel(title: 'الاسم الأول'),
              RoundedTextField(
                text: userModel!.firstName.trim(),
                onChange: (newText) {
                  userModel!.firstName = newText.trim();
                  if (context.read<ShowCautionBloc>().state
                      is ShowCautionTrueState) {
                    context
                        .read<ShowCautionBloc>()
                        .add(ShowCautionFalseEvent());
                  }
                },
              ),
              SizedBox(height: 15),
              MyLabel(title: 'الاسم الأخير'),
              RoundedTextField(
                text: userModel!.lastName.trim(),
                onChange: (newText) {
                  userModel!.lastName = newText.trim();
                  if (context.read<ShowCautionBloc>().state
                      is ShowCautionTrueState) {
                    context
                        .read<ShowCautionBloc>()
                        .add(ShowCautionFalseEvent());
                  }
                },
              ),
              SizedBox(height: 15),
              MyLabel(title: 'البريد الإلكتروني'),
              RoundedTextField(
                text: userModel!.username.trim(),
                onChange: (newText) {
                  userModel!.username = newText.trim();
                  if (context.read<ShowCautionBloc>().state
                      is ShowCautionTrueState) {
                    context
                        .read<ShowCautionBloc>()
                        .add(ShowCautionFalseEvent());
                  }
                },
              ),
              SizedBox(height: 15),
              MyLabel(title: 'رقم الجوال'),
              RoundedTextField(
                text: userModel!.number.trim(),
                onChange: (newText) {
                  userModel!.number = newText.trim();
                  if (context.read<ShowCautionBloc>().state
                      is ShowCautionTrueState) {
                    context
                        .read<ShowCautionBloc>()
                        .add(ShowCautionFalseEvent());
                  }
                },
              ),
              SizedBox(height: 30),
              BlocBuilder<ShowCautionBloc, ShowCautionState>(
                builder: (context, state) {
                  if (state is ShowCautionTrueState) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      // TODO: Animated text
                      child: Text(
                        '* تأكد من ملء جميع الحقول !',
                        textDirection: TextDirection.rtl,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: MediaQuery.of(context).size.height / 45,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    );
                  }
                  return Container();
                },
              ),
              MyButton(
                text: (userId == null) ? 'إضافة' : 'تعديل',
                onPressed: () {
                  if (userId != null) {
                    FireBaseConstants.instance
                        .collection(FireBaseConstants.usersCollection)
                        .doc(userId)
                        .set({
                      FireBaseConstants.userUsername:
                          userModel!.username.toLowerCase(),
                      FireBaseConstants.userFirstName:
                          userModel!.firstName.toLowerCase(),
                      FireBaseConstants.userLastName:
                          userModel!.lastName.toLowerCase(),
                      FireBaseConstants.userNumber: userModel!.number,
                      FireBaseConstants.userStatus: userModel!.status,
                    });
                    ScaffoldMessenger.of(context).showSnackBar(
                      MySnackBar(message: 'تم تعديل المستخدم بنجاح'),
                    );
                  } else {
                    if (userModel!.firstName.isEmpty ||
                        userModel!.lastName.isEmpty ||
                        userModel!.username.isEmpty ||
                        userModel!.number.isEmpty) {
                      context
                          .read<ShowCautionBloc>()
                          .add(ShowCautionTrueEvent());
                      // showCaution = true;
                      return;
                    } else {
                      FireBaseConstants.instance
                          .collection(FireBaseConstants.usersCollection)
                          .add({
                        FireBaseConstants.userUsername:
                            userModel!.username.toLowerCase(),
                        FireBaseConstants.userFirstName:
                            userModel!.firstName.toLowerCase(),
                        FireBaseConstants.userLastName:
                            userModel!.lastName.toLowerCase(),
                        FireBaseConstants.userNumber: userModel!.number,
                        FireBaseConstants.userStatus: userModel!.status,
                      });
                      ScaffoldMessenger.of(context).showSnackBar(
                        MySnackBar(message: 'تم إضافة المستخدم بنجاح'),
                      );
                    }
                  }
                  Navigator.pop(context);
                },
                color: kGreenColor,
                textColor: Colors.white,
                isPressed: true,
                elevation: 5,
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
