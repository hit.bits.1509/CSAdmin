import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/widgets/my_bottom_sheet.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_text_field.dart';
import 'package:flutter/material.dart';

class AddSubject extends StatelessWidget {
  late String subjectName;

  // late String hTag;

  AddSubject();

  @override
  Widget build(BuildContext context) {
    return MyBottomSheet(
      title: 'إضافة مادة',
      onButtonPressed: () async {
        if (subjectName != ' ') {
          ScaffoldMessenger.of(context).showSnackBar(
            MySnackBar(message: 'تم إضافة المادة بنجاح'),
          );
          await FireBaseConstants.instance
              .collection(FireBaseConstants.subjectsCollection)
              .doc(subjectName)
              .set({'temp': 'temp'});
        }
        Navigator.pop(context);
      },
      child: MyTextField(
        hint: '...اسم المادة',
        controller: TextEditingController(),
        autofocus: true,
        onChanged: (newText) {
          subjectName = newText;
        },
      ),
    );
  }
}
