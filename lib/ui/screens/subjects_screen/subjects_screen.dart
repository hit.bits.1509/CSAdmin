import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/screens/departments_screen/department_screen.dart';
import 'package:csadmin/ui/screens/subjects_screen/add_subject.dart';
import 'package:csadmin/ui/widgets/app_bars/dynamic_app_bar.dart';
import 'package:csadmin/ui/widgets/loading_indicator.dart';
import 'package:csadmin/ui/widgets/my_alert_dailog.dart';
import 'package:csadmin/ui/widgets/my_card.dart';
import 'package:csadmin/ui/widgets/my_fab.dart';
import 'package:csadmin/ui/widgets/my_snack_bar.dart';
import 'package:csadmin/ui/widgets/my_toggle_appbar_detector.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:page_transition/page_transition.dart';

class SubjectsScreen extends StatefulWidget {
  const SubjectsScreen();

  @override
  State<SubjectsScreen> createState() => _SubjectsScreenState();
}

class _SubjectsScreenState extends State<SubjectsScreen> {
  dynamic data;
  late int selectedIndex;
  late BuildContext thisContext;
  late String subjectName;

  @override
  void initState() {
    subjectsAppBarBloc = AppBarBloc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AppBarBloc>(
      create: (context) => subjectsAppBarBloc!,
      child: BlocBuilder<AppBarBloc, AppBarState>(
        builder: (context, state) {
          return MyToggleAppBarDetector(
            context: context,
            bloc: subjectsAppBarBloc,
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: DynamicAppBar(
                title: 'الــمواد',
                appBarBloc: subjectsAppBarBloc!,
                onCopy: () {
                  Clipboard.setData(ClipboardData(text: subjectName));
                  toggleAppBer(thisContext);
                  ScaffoldMessenger.of(context).showSnackBar(
                    MySnackBar(
                      message: 'تم نسخ اسم المادة إلى الحافظة',
                      icon: FontAwesomeIcons.clipboardCheck,
                    ),
                  );
                },
                onDelete: () {
                  showDialog(
                    context: context,
                    builder: (context) => MyAlertDialog(
                      message:
                          'هل انت متاكد من حذف مادة ال$subjectName نهائيا؟',
                      onYes: () async {
                        FireBaseConstants.instance
                            .collection(FireBaseConstants.subjectsCollection)
                            .doc(subjectName)
                            .delete();
                        Navigator.pop(context);
                        toggleAppBer(thisContext);
                        ScaffoldMessenger.of(context).showSnackBar(
                          MySnackBar(message: 'تم حذف المادة'),
                        );
                      },
                      onNo: () {
                        Navigator.pop(context);
                      },
                    ),
                  );
                },
              ),
              body: WillPopScope(
                onWillPop: () async {
                  if (!shouldPop) {
                    toggleAppBer(thisContext);
                    return false;
                  } else {
                    return shouldPop;
                  }
                },
                child: SafeArea(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FireBaseConstants.instance
                        .collection(FireBaseConstants.subjectsCollection)
                        .snapshots(),
                    builder: (context, snapshot) {
                      thisContext = context;
                      if (snapshot.hasData) {
                        final data = snapshot.requireData;
                        return GridView.builder(
                          padding: const EdgeInsets.all(kMarginCard),
                          itemCount: data.size,
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            mainAxisSpacing: kPaddingCard,
                            crossAxisSpacing: kPaddingCard,
                            crossAxisCount: 2,
                          ),
                          itemBuilder: (context, index) {
                            return BlocBuilder<AppBarBloc, AppBarState>(
                              builder: (context, state) {
                                final String subject = data.docs[index].id;
                                return Hero(
                                  tag: 'card$index',
                                  child: MyCard(
                                    backgroundColor: state is AppBarMenuState &&
                                            (index == selectedIndex)
                                        ? kNiceBlueColor.withOpacity(0.4)
                                        : kLightBlueColor,
                                    onLongTap: () {
                                      selectedIndex = index;
                                      subjectName = subject;
                                      toggleAppBer(context);
                                    },
                                    onTap: shouldPop
                                        ? () async {
                                            selectedIndex = index;
                                            Navigator.push(
                                              context,
                                              PageTransition(
                                                child: DepartmentScreen(
                                                  subjectName: subject,
                                                ),
                                                type: PageTransitionType
                                                    .rightToLeft,
                                                duration: myDuration,
                                              ),
                                            );
                                            FireBaseConstants.instance
                                                .collection(FireBaseConstants
                                                    .subjectsCollection)
                                                .doc(subject)
                                                .update({
                                              FireBaseConstants.temp:
                                                  FieldValue.delete()
                                            });
                                          }
                                        : () {
                                            toggleAppBer(context);
                                          },
                                    iconData: FontAwesomeIcons.book,
                                    text1: subject,
                                  ),
                                );
                              },
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        return const Center(child: Text('Error'));
                      }
                      return const Center(child: LoadingIndicator());
                    },
                  ),
                ),
              ),
              floatingActionButton: MyFAB(
                onPressed: () {
                  if (state is AppBarMenuState) {
                    toggleAppBer(thisContext);
                  }
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) => AddSubject(),
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    subjectsAppBarBloc!.close();
  }
}
