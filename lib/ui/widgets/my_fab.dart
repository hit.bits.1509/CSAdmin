import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyFAB extends StatelessWidget {
  final VoidCallback onPressed;
  const MyFAB({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      elevation: 5,
      backgroundColor: kNiceGreenColor,
      child: const Icon(
        FontAwesomeIcons.plus,
        size: 20,
      ),
      onPressed: onPressed,
    );
  }
}
