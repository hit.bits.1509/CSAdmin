import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class TextAlignIcons extends StatelessWidget {
  const TextAlignIcons({
    required this.imagePath,
    required this.color,
  });

  final String imagePath;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(MediaQuery.of(context).size.width / 200),
      width: MediaQuery.of(context).size.width / 5,
      height: 50,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: kBorderGreyColor,
          width: 2,
        ),
        boxShadow: const [
          BoxShadow(
            color: kBorderGreyColor,
          ),
        ],
      ),
      child: Image(
        image: AssetImage(imagePath),
      ),
    );
  }
}
