import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/ui/screens/home_screen/bloc/tab_bar_bloc.dart';
import 'package:csadmin/ui/screens/home_screen/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_button.dart';

enum AppScreens {
  dashboard,
  manage,
}

class MyTabBar extends StatefulWidget {
  final PageController controller;
  const MyTabBar({required this.controller});

  @override
  State<MyTabBar> createState() => _MyTabBarState();
}

class _MyTabBarState extends State<MyTabBar> {
  AppScreens appScreens = AppScreens.dashboard;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(15),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
        border: Border.all(
          color: kBorderGreyColor,
          width: 2,
        ),
        boxShadow: const [
          BoxShadow(
            color: kBorderGreyColor,
          ),
        ],
      ),
      child: BlocBuilder<TabBarBloc, TabBarState>(
        builder: (context, state) {
          if (state is DashBoardState) {
            appScreens = AppScreens.dashboard;
          } else if (state is ManageState) {
            appScreens = AppScreens.manage;
          }
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              MyButton(
                text: "Dashboard",
                height: 37,
                textColor: appScreens == AppScreens.manage
                    ? kBorderGreyColor
                    : Colors.white,
                color: appScreens == AppScreens.manage
                    ? Colors.white
                    : kNiceGreenColor,
                onPressed: () {
                  // context.read<TabBarBloc>().add(DashBoardEvent());
                  controller.animateToPage(
                    0,
                    curve: Curves.ease,
                    duration: Duration(
                      milliseconds: 600,
                    ),
                  );
                },
                isPressed: appScreens == AppScreens.dashboard,
                elevation: appScreens == AppScreens.manage ? 0 : 2,
              ),
              MyButton(
                text: "Manage",
                height: 37,
                textColor: appScreens == AppScreens.dashboard
                    ? kBorderGreyColor
                    : Colors.white,
                color: appScreens == AppScreens.dashboard
                    ? Colors.white
                    : kNiceGreenColor,
                onPressed: () {
                  // context.read<TabBarBloc>().add(ManageEvent());
                  controller.animateToPage(
                    1,
                    curve: Curves.ease,
                    duration: Duration(
                      milliseconds: 600,
                    ),
                  );
                },
                isPressed: appScreens == AppScreens.manage,
                elevation: appScreens == AppScreens.dashboard ? 0 : 2,
              ),
            ],
          );
        },
      ),
    );
  }
}
