import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyAlertDialog extends StatelessWidget {
  final Widget? title;
  final String message;
  final VoidCallback onYes;
  final VoidCallback onNo;
  const MyAlertDialog({
    Key? key,
    this.title,
    required this.message,
    required this.onYes,
    required this.onNo,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 5,
      actionsAlignment: MainAxisAlignment.spaceAround,
      title: title ??
          const FaIcon(
            FontAwesomeIcons.exclamationTriangle,
            color: kNiceYellowColor,
            textDirection: TextDirection.rtl,
          ),
      content: Text(
        message,
        textDirection: TextDirection.rtl,
      ),
      contentPadding: const EdgeInsets.all(20.0),
      actions: [
        MaterialButton(
          onPressed: onYes,
          splashColor: Colors.red,
          child: const Text(
            'حذف',
            style: TextStyle(
              color: Colors.red,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        MaterialButton(
          onPressed: onNo,
          textColor: Colors.blue,
          splashColor: Colors.blue,
          child: const Text(
            'إلغاء',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        )
      ],
    );
  }
}
