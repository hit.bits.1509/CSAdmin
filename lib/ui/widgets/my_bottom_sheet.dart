// ignore_for_file: must_be_immutable
import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class MyBottomSheet extends StatelessWidget {
  final Widget child;
  final VoidCallback? onButtonPressed;
  final String? buttonText;
  final String title;
  const MyBottomSheet({
    Key? key,
    required this.child,
    required this.title,
    this.onButtonPressed,
    this.buttonText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xff757575),
      padding: MediaQuery.of(context).viewInsets,
      child: Container(
        // height: 550,
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 40),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            topRight: Radius.circular(30),
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              const Divider(
                color: Colors.grey,
                thickness: 3,
                indent: 122,
                endIndent: 122,
              ),
              Text(
                title,
                style: const TextStyle(
                  color: kNiceGreenColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                ),
              ),
              const SizedBox(height: 20),
              child,
              const SizedBox(height: 20),
              MaterialButton(
                onPressed: onButtonPressed,
                color: kNiceGreenColor,
                minWidth: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Text(
                    buttonText ?? 'إضـافــة',
                    style: const TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
              // MaterialButton(
              //   onPressed: () {
              //     Navigator.pop(context);
              //   },
              //   color: kNiceYellowColor,
              //   minWidth: double.infinity,
              //   child: const Padding(
              //     padding: EdgeInsets.all(4.0),
              //     child: Text(
              //       'إلغاء',
              //       style: TextStyle(
              //         fontWeight: FontWeight.w700,
              //         fontSize: 20,
              //         color: Colors.white,
              //       ),
              //     ),
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }
}
