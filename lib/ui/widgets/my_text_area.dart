import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class MyTextArea extends StatelessWidget {
  final Function(String) onChanged;
  final String hint;
  final int lines;
  TextAlign? textAlign;
  final TextEditingController controller;

  MyTextArea({
    this.textAlign = TextAlign.center,
    required this.onChanged,
    required this.hint,
    required this.lines,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 0, vertical: 3),
      child: TextField(
        minLines: lines,
        maxLines: 20,
        textDirection: (textAlign == TextAlign.right)
            ? TextDirection.rtl
            : TextDirection.ltr,
        textAlign: TextAlign.center,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          // fillColor: color,
          // filled: true,
          hintText: hint,
          hintStyle: const TextStyle(color: Colors.grey),
          contentPadding: const EdgeInsets.all(5),
        ),
        onChanged: onChanged,
        controller: controller,
        cursorColor: kNiceYellowColor,
      ),
    );
  }
}
