// ignore_for_file: must_be_immutable
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/models/question_model.dart';
import 'package:csadmin/ui/screens/question_screen/bloc/checkbox_cubit_cubit.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'my_list_tile.dart';

class MyChoiceTile extends StatelessWidget {
  final int index;
  final Question question;
  final String letter;
  final TextAlign textAlign;
  final TextEditingController controller;

  const MyChoiceTile({
    this.textAlign = TextAlign.right,
    required this.question,
    required this.index,
    required this.letter,
    required this.controller,
  });

  @override
  Widget build(BuildContext context) {
    return MyListTile(
      onTap: () {},
      headIconButton: Container(
        child: Center(
          child: Text(
            letter,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 20,
            ),
          ),
        ),
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: kNiceBlueColor,
          borderRadius: BorderRadius.circular(6),
          border: Border.all(
            color: kBorderGreyColor,
            width: 1,
          ),
          boxShadow: const [
            BoxShadow(
              color: kBorderGreyColor,
            ),
          ],
        ),
      ),
      title: TextField(
        onChanged: (val) {
          question.choices[index].text = val;
        },
        textAlign: textAlign,
        textDirection: (textAlign == TextAlign.right)
            ? TextDirection.rtl
            : TextDirection.ltr,
        decoration: const InputDecoration(
          border: InputBorder.none,
          hintStyle: TextStyle(color: Colors.grey),
          contentPadding: EdgeInsets.all(5),
        ),
        controller: controller,
      ),
      tailIconButton: BlocBuilder<CheckboxCubit, CheckboxState>(
        builder: (context, state) {
          return GestureDetector(
            onTap: () {
              context
                  .read<CheckboxCubit>()
                  .toggleChecboxValue(question.choices[index].isTrue);
              question.choices[index].toggleTrue();
            },
            child: Container(
              color: Colors.transparent,
              padding: const EdgeInsets.all(17),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: kDarkGreenColor,
                    width: 2,
                  ),
                  borderRadius: BorderRadius.circular(50),
                ),
                width: 22,
                height: 22,
                child: Checkbox(
                    side: const BorderSide(color: Colors.transparent),
                    shape: const CircleBorder(),
                    activeColor: kNiceGreenColor,
                    checkColor: kDarkGreenColor,
                    value: question.choices[index].isTrue,
                    onChanged: (value) {
                      context
                          .read<CheckboxCubit>()
                          .toggleChecboxValue(question.choices[index].isTrue);
                      question.choices[index].toggleTrue();
                    }),
              ),
            ),
          );
        },
      ),
    );
  }
}
