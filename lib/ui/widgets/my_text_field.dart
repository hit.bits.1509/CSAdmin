import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class MyTextField extends StatelessWidget {
  final String hint;
  final bool? autofocus;
  final Function(String) onChanged;
  final TextEditingController? controller;
  final TextAlign? textAlign;

  const MyTextField({
    Key? key,
    required this.hint,
    required this.onChanged,
    this.controller,
    this.autofocus,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: controller,
      decoration: InputDecoration(
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(color: kBorderGreyColor, width: 2),
        ),
        hintText: hint,
        hintStyle: const TextStyle(color: Colors.grey, fontSize: 16),
      ),
      cursorColor: kNiceYellowColor,
      autofocus: autofocus ?? false,
      textAlign: textAlign ?? TextAlign.center,
      textDirection: TextDirection.rtl,
      onChanged: onChanged,
      cursorHeight: 30,
      style: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w400,
      ),
    );
  }
}
