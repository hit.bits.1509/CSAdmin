import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MyCard extends StatelessWidget {
  final IconData? iconData;
  final Color? backgroundColor;
  final String text1;
  final int lines;
  final String? text2;
  final String? text3;
  final String? text4;
  final VoidCallback onTap;
  final VoidCallback? onLongTap;
  final VoidCallback? onDoubleTap;

  const MyCard({
    Key? key,
    required this.text1,
    required this.onTap,
    this.iconData,
    this.onLongTap,
    this.text2,
    this.text3,
    this.text4,
    this.backgroundColor,
    this.onDoubleTap,
    this.lines = 1,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      onLongPress: onLongTap,
      onDoubleTap: onDoubleTap,
      child: Card(
        color: backgroundColor ?? kLightBlueColor,
        elevation: 2,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: const BorderSide(
            color: kBorderGreyColor,
            width: 2,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: FaIcon(
                      iconData ?? FontAwesomeIcons.android,
                      size: 32,
                      color: kNiceBlueColor,
                    ),
                  ),
                ],
              ),
              SizedBox(height: (lines == 1) ? 15 : 8),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        text1,
                        overflow: TextOverflow.ellipsis,
                        style: const TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w700,
                          color: kBorderGreyColor,
                        ),
                      ),
                      const SizedBox(height: 3),
                      if (lines > 1)
                        Column(
                          children: [
                            Text(
                              text2 ?? ' ',
                              style: const TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: kBorderGreyColor,
                              ),
                            ),
                            const SizedBox(height: 2),
                            Text(
                              text3 ?? ' ',
                              style: const TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: kBorderGreyColor,
                              ),
                            ),
                            const SizedBox(height: 2),
                            Text(
                              text4 ?? ' ',
                              style: const TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w700,
                                color: kNiceGreenColor,
                              ),
                            ),
                          ],
                        )
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
