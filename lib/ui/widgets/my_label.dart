import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MyLabel extends StatelessWidget {
  String title;
  MyLabel({required this.title});

  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      textDirection: TextDirection.rtl,
      textAlign: TextAlign.right,
      style: const TextStyle(
        color: Colors.black54,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
