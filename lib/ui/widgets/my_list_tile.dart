import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class MyListTile extends StatelessWidget {
  final VoidCallback? onTap;
  final Widget? headIconButton;
  final Widget? tailIconButton;
  final Color? tileColor;
  final Widget title;
  final Widget? subTitle;

  const MyListTile({
    Key? key,
    required this.title,
    this.onTap,
    this.headIconButton,
    this.tailIconButton,
    this.subTitle,
    this.tileColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: ListTile(
        tileColor: tileColor ?? kLightBlueColor,
        leading: headIconButton,
        trailing: tailIconButton,
        title: title,
        subtitle: subTitle,
        enabled: true,
        onTap: onTap,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: const BorderSide(color: kBorderGreyColor, width: 1.5),
        ),
      ),
    );
  }
}
