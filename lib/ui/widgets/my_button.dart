import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color color;
  final Color textColor;
  final bool isPressed;
  final double elevation;
  final double? width;
  final double? height;

  const MyButton({
    Key? key,
    required this.text,
    required this.onPressed,
    required this.color,
    required this.textColor,
    required this.isPressed,
    required this.elevation,
    this.width,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      height: height ?? 50,
      minWidth: width ?? 150,
      elevation: elevation,
      onPressed: onPressed,
      disabledColor: Colors.transparent,
      child: Text(
        text,
        style: TextStyle(
          color: textColor,
          fontWeight: FontWeight.w800,
          fontSize: 18,
        ),
      ),
      color: color,
      splashColor: color,
      disabledElevation: 0,
      shape: !isPressed
          ? null
          : RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              side: const BorderSide(
                color: kBorderGreyColor,
                width: 2,
              ),
            ),
    );
  }
}
