// ignore_for_file: must_be_immutable
import 'package:csadmin/core/blocs/text_align_bloc/bloc/text_align_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:csadmin/ui/widgets/text_align_action/text_align_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
 
class MyAppBar extends StatelessWidget with PreferredSizeWidget {
  final bool canPop;
  final String title;
  final VoidCallback? onActionPressed;
  final AppBarAction? action;

  const MyAppBar({
    required this.title,
    required this.canPop,
    this.action,
    this.onActionPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'appBar',
      child: BlocProvider.value(
        value: textAlignBloc,
        child: BlocBuilder<TextAlignBloc, TextAlignState>(
          builder: (context, state) {
            return AppBar(
              centerTitle: true,
              backgroundColor: Colors.white,
              foregroundColor: kBorderGreyColor,
              elevation: 0,
              leading: canPop
                  ? IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      icon: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: FaIcon(
                          FontAwesomeIcons.angleLeft,
                          size: 30,
                        ),
                      ),
                    )
                  : null,
              title: Text(
                title,
                style: const TextStyle(
                  color: Colors.black54,
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                ),
              ),
              actions: (action == AppBarAction.textAlign)
                  ? [
                      IconButton(
                        onPressed: () {
                          context
                              .read<TextAlignBloc>()
                              .add(TextAlignLtrEvent());
                        },
                        icon: TextAlignIcons(
                          imagePath: 'assets/in_app_icons/textalign-left.png',
                          color: (state is TextAlignLtrState)
                              ? kNiceGreenColor
                              : Colors.white,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          context
                              .read<TextAlignBloc>()
                              .add(TextAlignRtlEvent());
                        },
                        icon: TextAlignIcons(
                          imagePath: 'assets/in_app_icons/textalign-right.png',
                          color: (state is TextAlignRtlState)
                              ? kNiceGreenColor
                              : Colors.white,
                        ),
                      ),
                    ]
                  : (action == AppBarAction.refreshPage)
                      ? [
                          IconButton(
                            onPressed: onActionPressed,
                            icon: Icon(
                              Icons.cancel_outlined,
                              color: kBorderGreyColor,
                            ),
                          ),
                        ]
                      : null,
            );
          },
        ),
      ),
    );
  }

  @override
  Size get preferredSize => _PreferredAppBarSize(null, null);
}

class _PreferredAppBarSize extends Size {
  _PreferredAppBarSize(this.toolbarHeight, this.bottomHeight)
      : super.fromHeight(
          (toolbarHeight ?? kToolbarHeight) + (bottomHeight ?? 0),
        );

  final double? toolbarHeight;
  final double? bottomHeight;
}
