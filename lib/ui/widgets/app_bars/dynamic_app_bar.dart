import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/utilities/constants.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:csadmin/ui/widgets/app_bars/action_enum.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'my_app_bar.dart';

class DynamicAppBar extends StatelessWidget with PreferredSizeWidget {
  final String title;
  final AppBarBloc appBarBloc;
  final VoidCallback? onDelete;
  final VoidCallback? onUpdate;
  final VoidCallback? onCopy;
  final VoidCallback? onActionPressed;
  final AppBarAction? appBarAction;

  const DynamicAppBar({
    required this.title,
    required this.appBarBloc,
    this.appBarAction,
    this.onActionPressed,
    this.onDelete,
    this.onUpdate,
    this.onCopy,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AppBarBloc, AppBarState>(
      builder: (context, state) {
        if (state is AppBarMenuState) {
          return AppBar(
            key: key,
            centerTitle: true,
            backgroundColor: kLightBlueColor,
            foregroundColor: kBorderGreyColor,
            elevation: 0,
            leading: IconButton(
              icon: const Icon(Icons.cancel),
              onPressed: () {
                toggleAppBer(context);
              },
            ),
            actions: [
              IconButton(
                onPressed: onCopy,
                icon: const FaIcon(FontAwesomeIcons.copy),
              ),
              if (onUpdate != null)
                IconButton(
                  onPressed: onUpdate,
                  icon: const FaIcon(FontAwesomeIcons.pencilAlt),
                ),
              IconButton(
                onPressed: onDelete,
                icon: const FaIcon(FontAwesomeIcons.trashAlt),
              ),
            ],
          );
        } else {
          return BlocProvider<AppBarBloc>(
            create: (context) => appBarBloc,
            child: MyAppBar(
              onActionPressed: onActionPressed,
              action: appBarAction,
              title: title,
              canPop: true,
            ),
          );
        }
      },
    );
  }

  @override
  Size get preferredSize => _PreferredAppBarSize(null, null);
}

class _PreferredAppBarSize extends Size {
  _PreferredAppBarSize(this.toolbarHeight, this.bottomHeight)
      : super.fromHeight(
          (toolbarHeight ?? kToolbarHeight) + (bottomHeight ?? 0),
        );

  final double? toolbarHeight;
  final double? bottomHeight;
}
