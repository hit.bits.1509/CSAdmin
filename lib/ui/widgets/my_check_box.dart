import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MyCheckBox extends StatelessWidget {
  bool isActive;
  Function(bool) onCheck;
  MyCheckBox({
    required this.isActive,
    required this.onCheck,
  });

  @override
  Widget build(BuildContext context) {
    return Checkbox(
      value: isActive,
      onChanged: (val) {},
      side: BorderSide(color: Colors.black26, width: 2),
      shape: const CircleBorder(),
      activeColor: Color(0xFF2BBF5D),
      checkColor: kDarkGreenColor,
    );
  }
}
