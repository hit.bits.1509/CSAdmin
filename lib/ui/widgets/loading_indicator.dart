import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        const SpinKitFadingCircle(
          color: kNiceBlueColor,
          size: 70,
        ),
        Text(
          'Loading ...',
          style: TextStyle(
              color: kBorderGreyColor,
              fontSize: MediaQuery.of(context).size.height / 45),
        ),
      ],
    );
  }
}
