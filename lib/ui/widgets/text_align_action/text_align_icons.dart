import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';

class TextAlignIcons extends StatelessWidget {
  const TextAlignIcons({
    required this.imagePath,
    required this.color,
  });

  final String imagePath;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(2),
      // width: 50,
      height: 32,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          color: kBorderGreyColor,
          width: 2,
        ),
      ),
      child: Image(
        image: AssetImage(imagePath),
      ),
    );
  }
}
