import 'package:csadmin/core/utilities/globals.dart';
import 'package:flutter/material.dart';

class MyPopDetector extends StatelessWidget {
  final Widget child;
  const MyPopDetector({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (val) {
        if (val.primaryVelocity! > 0 && shouldPop) {
          Navigator.pop(context);
        }
      },
      child: child,
    );
  }
}
