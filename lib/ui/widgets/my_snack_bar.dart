import 'package:csadmin/core/utilities/constants.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MySnackBar extends SnackBar {
  // final String message;
  // final IconData? icon;
  MySnackBar({
    Key? key,
    required String message,
    IconData? icon,
  }) : super(
          key: key,
          backgroundColor: kNiceGreenColor,
          content: Row(
            textDirection: TextDirection.rtl,
            children: [
              const Spacer(),
              FaIcon(
                icon ?? FontAwesomeIcons.checkCircle,
                color: Colors.white,
              ),
              const SizedBox(width: 8),
              Text(
                message,
                textAlign: TextAlign.right,
                style: const TextStyle(fontWeight: FontWeight.bold),
              ),
              const Spacer(),
            ],
          ),
        );
}
