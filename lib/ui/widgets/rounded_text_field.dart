import 'package:flutter/material.dart';

// ignore: must_be_immutable
class RoundedTextField extends StatelessWidget {
  bool autofocus;
  String? text;
  Function(String) onChange;
  RoundedTextField({
    this.text,
    this.autofocus = false,
    required this.onChange,
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      textAlign: TextAlign.left,
      onChanged: onChange,
      autofocus: autofocus,
      controller: TextEditingController(text: text),
      textDirection: TextDirection.ltr,
      decoration: const InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red),
          borderRadius: BorderRadius.horizontal(
            left: Radius.circular(15),
            right: Radius.circular(15),
          ),
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 25,
          vertical: 5,
        ),
      ),
    );
  }
}
