import 'package:bloc/bloc.dart';
import 'package:csadmin/core/blocs/app_bar_bloc/bloc/app_bar_bloc.dart';
import 'package:csadmin/core/utilities/globals.dart';
import 'package:flutter/material.dart';

class MyToggleAppBarDetector extends StatelessWidget {
  final Widget child;
  final BuildContext context;
  final Bloc? bloc;
  const MyToggleAppBarDetector({
    required this.child,
    required this.context,
    this.bloc,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (val) {
        if (val.primaryVelocity! > 0 && shouldPop) {
          Navigator.pop(context);
        }
      },
      onTap: () {
        if (bloc!.state is AppBarMenuState) {
          toggleAppBer(context);
        }
      },
      child: child,
    );
  }
}
